﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YulpApi.Models;

namespace YulpApi.Controllers
{
    [Route("api/star")]
    [ApiController]
    public class StarController : ControllerBase
    {
        private readonly YulpContext _context;

        public StarController(YulpContext context)
        {
            _context = context;

        }

        // GET: api/star
        [HttpGet("{star}")]
        public async Task<ActionResult<IEnumerable<YulpItem>>> GetStarItems(long star)
        {
            Task<List<YulpItem>> yulps = _context.YulpItems.Where<YulpItem>(item => item.Star == star).ToListAsync();
            return await yulps;
        }
    }
}