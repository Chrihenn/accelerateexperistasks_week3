﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YulpApi.Models;

namespace YulpApi.Controllers
{
    [Route("api/yulp")]
    [ApiController]
    public class YulpController : ControllerBase
    {
        private readonly YulpContext _context;

        public YulpController(YulpContext context)
        {
            _context = context;

            if (_context.YulpItems.Count() == 0)
            {
                _context.YulpItems.Add(new YulpItem { Name = "Item1" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<YulpItem>>> GetYulpItems()
        {
            return await _context.YulpItems.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<YulpItem>> GetYulpItem(long id)
        {
            var yulpItem = await _context.YulpItems.FindAsync(id);

            if (yulpItem == null)
            {
                return NotFound();
            }

            return yulpItem;
        }

        [HttpPost]
        public async Task<ActionResult<YulpItem>> PostYulpItem(YulpItem item)
        {
            if (item.Reviewer == "")
            {
                item.Reviewer = "anonymous";
            }
          
            _context.YulpItems.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetYulpItem), new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutYulpItem(long id, YulpItem item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteYulpItem(long id)
        {
            var YulpItem = await _context.YulpItems.FindAsync(id);

            if (YulpItem == null)
            {
                return NotFound();
            }

            _context.YulpItems.Remove(YulpItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}