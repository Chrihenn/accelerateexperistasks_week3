﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YulpApi.Models;

namespace YulpApi.Controllers
{
    [Route("api/reviewer")]
    [ApiController]
    public class ReviewerController : ControllerBase
    {
        private readonly  YulpContext _context;

        public ReviewerController(YulpContext context)
        {
            _context = context;

        }

        [HttpGet("{reviewer}")]
        public async Task<ActionResult<IEnumerable<YulpItem>>> GetStarItems(string reviewer)
        {
            Task<List<YulpItem>> yulps = _context.YulpItems.Where<YulpItem>(item => item.Reviewer == reviewer).ToListAsync();
            return await yulps;
        }
    }
}