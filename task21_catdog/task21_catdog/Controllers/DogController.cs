﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using task21_catdog.Models;

namespace task21_catdog.Controllers
{
    public class DogController : Controller
    {
        public IActionResult Dog()
        {
            var url = GetReleases();
            ViewBag.url = url;
            return View();
        }

        public string GetReleases()
        {
            var client = new WebClient();
            string url = "https://random.dog/woof.json";
            var response = client.DownloadString(url);
            dynamic dyn = JsonConvert.DeserializeObject(response);
            string fileurl = dyn["url"];
            return fileurl;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}