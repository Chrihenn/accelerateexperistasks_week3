﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using task22_catdog_game.Models;

namespace task22_catdog_game.Controllers
{
    public class HomeController : Controller
    {
        public int scoreCounter;
        public IActionResult Index()
        {
            Random rnd = new Random();

            var url = "";

            int ii = rnd.Next(2);

            if (ii == 0)
            {
                url = GetDogImage();
                SetCookie("animal", "dog");
            }
            else if (ii == 1)
            {
                url = GetCatImage();
                SetCookie("animal", "cat");
            }
            ViewBag.url = url;
            return View();
        }

        public string GetDogImage()
        {
            var client = new WebClient();
            string url = "https://random.dog/woof.json";
            var response = client.DownloadString(url);
            dynamic dyn = JsonConvert.DeserializeObject(response);
            string fileurl = dyn["url"];
            return fileurl;
        }

        public string GetCatImage()
        {
            var client = new WebClient();
            string url = "http://aws.random.cat/meow";
            var response = client.DownloadString(url);
            dynamic dyn = JsonConvert.DeserializeObject(response);
            string fileurl = dyn["file"];
            return fileurl;
        }

        public IActionResult CheckButton(string button)
        {
            try
            {
                scoreCounter = Int32.Parse(Request.Cookies["score"]);
            }
            catch
            {
                scoreCounter = 0;
            }

            if (button == "dog")
            {
                TempData["buttonval"] = "Dog button pressed";


                if (Request.Cookies["animal"] == "dog")
                {
                    scoreCounter = scoreCounter + 1;
                }
                else
                {
                    scoreCounter = 0;
                }
            }
            else if (button == "cat")
            {
                TempData["buttonval"] = "Cat button pressed";


                if (Request.Cookies["animal"] == "cat")
                {
                    scoreCounter = scoreCounter + 1;
                    
                }
                else
                {
                    scoreCounter = 0;
                }
            }
            bool result = DidUserWin(scoreCounter);
            if (result)
            {
                TempData["victory"] = "YOU WIN!";
            }
            TempData["score"] = scoreCounter;
            SetCookie("score", scoreCounter.ToString());
            return RedirectToAction("Index");
        }

        public bool DidUserWin(int score)
        {
            if(score >= 11)
            {
                return true;
            } else
            {
                return false;
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void SetCookie(string Key, string Value)
        {
            CookieOptions option = new CookieOptions();
            Response.Cookies.Append(Key, Value);
        }
    }
}
