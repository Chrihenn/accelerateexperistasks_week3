﻿namespace YulpApi.Models
{
    public class YulpItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public long Star { get; set; }
        public string Reviewer { get; set; }
    }
}
