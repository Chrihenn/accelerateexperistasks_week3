﻿using Microsoft.EntityFrameworkCore;

namespace YulpApi.Models
{
    public class YulpContext : DbContext
    {
        public YulpContext(DbContextOptions<YulpContext> options)
            : base(options)
        {
        }

        public DbSet<YulpItem> YulpItems { get; set; }
    }
}
